/**
 * FrameTest5.java
 * 
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FrameTest5 {

	/**
	 * Esta es una clase desplega un Frame y le agrega componentes en un Panel
         * con un BorderLayout definido a la etiqueta y al momento de agregarlo
	 * @version 1.0
	 * @author Ivan Fuentes Q.
 	 */

	public static void main(String[] args) {
           	JFrame miFrame = new JFrame("Cawobunga!!!");

           	miFrame.setSize(400, 400);  

		//Tcnica para centrar el frame	
		Dimension frameSize = miFrame.getSize();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		miFrame.setLocation((screenSize.width - frameSize.width)/2, 
				         (screenSize.height - frameSize.height)/2);

		WindowListener w = new WindowAdapter() {
 			public void windowClosing(WindowEvent evt) {
        	        	exitForm(evt);
            		}
		};

// 		Listener para cerrar la ventana
		miFrame.addWindowListener(w);

		//AGREGUE VARIOS COMPONENTES DEFINALES UN LAYAUT A CADA UNO
		//Y UBIQUELOS EN LAS DIFERENTES REGIONES DEL BORDERLAYOUT

		Container contentPane = miFrame.getContentPane( );
		JLabel stuff = new JLabel("Soy una etiqueta", JLabel.CENTER);
		contentPane.add(stuff, BorderLayout.NORTH);

		JLabel stuff1 = new JLabel("Soy una etiqueta", JLabel.CENTER);
		contentPane.add(stuff1, BorderLayout.CENTER);
		
		JLabel stuff2 = new JLabel("Soy una etiqueta", JLabel.CENTER);
		contentPane.add(stuff2, BorderLayout.SOUTH);

           	miFrame.setVisible(true);
      	}

	/**
	 * Metodo que cierra el frame desde le boton de close (X) 
	 * @param el evento de tipo WindowEvent que se registra
	 */

	private static void exitForm(WindowEvent evt) {
        	System.exit(0);
	}


}
