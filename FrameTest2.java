/**
 * FrameTest2.java
 *
 */

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;


public class FrameTest2 {

	/**
	 * Esta es una clase desplega un Frame y le agrega una etiqueta
	 * @version 1.0
	 * @author Ivn Fuentes Quiroz
 	 */

	public static void main(String[] args) {
	JFrame miFrame = new JFrame("Cawobunga!!!");
	JLabel etiqueta = new JLabel("un letrerito");
	JLabel nombre = new JLabel("Juan Pablo Ángel Parra Arévalo");
	JLabel universidad = new JLabel("Fundación Universitaria San Mateo");
	JLabel facultad = new JLabel("Facultad de Ingeniería y Afines");
	JLabel carrera = new JLabel("Ingeniería de Sistemas");
	JButton boton = new JButton("Pulse Aqu!");

	//Listener para cerrar la ventana
	WindowListener w = new WindowAdapter() {
 		public void windowClosing(java.awt.event.WindowEvent evt) {
                	exitForm(evt);
            	}
	};

	//Registro al Frame el Listener 
	miFrame.addWindowListener(w);
	miFrame.setSize(300,200);

	//Obtengo el contenedor para agregar componentes
	Container contenedor = miFrame.getContentPane();
	boton.setVisible(true);
	boton.setEnabled(true);
	nombre.setVisible(false);
	universidad.setVisible(false);
	facultad.setVisible(false);
	carrera.setVisible(false);
	
	contenedor.setLayout(new FlowLayout());
	contenedor.add(etiqueta);
	contenedor.add(boton);
	contenedor.add(nombre);
	contenedor.add(universidad);
	contenedor.add(facultad);
	contenedor.add(carrera);

	//REALICE LOS NECESARIO PARA MOSTRAR SUS DATOS EN LA TERMINAL
	//CADA VEZ QUE EL BOTON SEA PULSADO (DESHABILITE COMENTARIOS)

	   //Instancio un Listener
	   ActionListener l = new ActionListener() {
	   	public void actionPerformed(ActionEvent e) {
	   		nombre.setVisible(true);
	   		universidad.setVisible(true);
	   		facultad.setVisible(true);
	   		carrera.setVisible(true);
	   		mostrarDatos();
	   	}
	   };

	   //Registro el listener al objeto deseado
	   boton.addActionListener(l);



	//NOTESE QUE EL BOTON Y LA ETIQUETA SE ADIEREN AL CONTENEDOR,
	//PERO NO SE PRESENTAN LOS DOS, POR, INDIQUE POR QUE Y QUE LO RESOLVERIA

	//Tecnica para centrado de frame
	Dimension framesize = miFrame.getSize();
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	int centroX  = screenSize.width/2;
	int centroY  = screenSize.height/2;
	int ancho  = framesize.width/2;
	int alto  = framesize.height/2;
	miFrame.setLocation((centroX -ancho), (centroY - alto));

	//Pongo el Frame Visible
	miFrame.setVisible(true);

	//miFrame.pack();
	} //fin del main
	
	/**
	 * Metodo que cierra el frame desde le boton de close (X) 
	 * @param el evento de tipo WindowEvent que se registra
	 */

	private static void exitForm(WindowEvent evt) {
        	System.exit(0);
	}
	
	public static void mostrarDatos(){
        System.out.println("Juan Pablo Ángel Parra Arévalo\n\n"+
                           "Fundación Universitaria San Mateo\n"+
                           "Facultad de Ingeniería y Afines\n"+
                           "Ingeniería de Sistemas");
	}

}
