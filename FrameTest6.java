/**
 * FrameTest6.java
 * 
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FrameTest6 {
	public static void main(String[] args) {

           	JFrame miFrame = new JFrame("Cawobunga!!!");
           	miFrame.setSize(400, 400); 

		Dimension frameSize = miFrame.getSize();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		miFrame.setLocation((screenSize.width - frameSize.width)/2, 
			             (screenSize.height - frameSize.height)/2);


		WindowListener w = new WindowAdapter() {
 			public void windowClosing(WindowEvent evt) {
        	        	exitForm(evt);
            		}
		};

		//Listener para cerrar la ventana
		miFrame.addWindowListener(w);

		//MODIFIQUE EL GRID Y AGREGUE OTRO RENGLON DE MANERA QUE PUEDA 
		//TENER UNA DISTRIBUCION DE TABLA DE 4 X 2 DONDE PREGUNTE
		//LA PROFESION DE LA PERSONA

		//Asigno un Layout a mi contenedor
		Container contentPane = miFrame.getContentPane( );
		contentPane.setLayout(new GridLayout(4, 2));  // 4 rows, 2 cols.

		// Creamos algunas etiquetas que agregar

 		JLabel l = new JLabel("Nombre:");
 		JLabel l2 = new JLabel("Direccin:");
 		JLabel l3 = new JLabel("Cdula:");
 		JLabel l4 = new JLabel("Profesión");

		// TUn nuevo componente area de texto
 		JTextArea t = new JTextArea("Este es una compo de texto definido " +
					    "en una area de 6 x 20 pixeles" +
					    " es por eso que se llama TextArea " +
					    "estoy usando el contructor con texto integrado",
					    6, 20);

		t.setLineWrap(true);
		t.setWrapStyleWord(true);

		// Creando dos campos de Texto

 		JTextField t2 = new JTextField("Este es un campo de Texto de una saola lnea");
 		JTextField t3 = new JTextField("otro caompo de texto");
 		JTextArea t4 = new JTextArea("Área de texto");

 		// Agrego de forma ascendente, primero renglon, luego columna.

 		contentPane.add(l);    	// ren. 1, col. 1
 		contentPane.add(t);	// ren. 1, col. 2
 		contentPane.add(l2);   	// ren. 2, col. 1 (etc.)
 		contentPane.add(t2);
 		contentPane.add(l3);
 		contentPane.add(t3);
 		contentPane.add(l4);
 		contentPane.add(t4);
 
             	miFrame.setVisible(true);
         }
	/**
	 * Metodo que cierra el frame desde le boton de close (X) 
	 * @param el evento de tipo WindowEvent que se registra
	 */

	private static void exitForm(WindowEvent evt) {
        	System.exit(0);
	}
 
}
