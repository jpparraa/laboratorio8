/**
 * FrameTest.java
 * 
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FrameTest {

	/**
	 * Esta es una clase desplega un Frame (ventana principal)
	 * @version 1.0
	 * @author Ivn Fuentes Quiroz
 	 */

	public static void main(String[] args) {
	//Instancio un frame
	JFrame miFrame = new JFrame("¡¡¡Cawobunga!!!");
	
	//OBTENGA EL CONTENEDOR DE FRAME Y CREE UN COMPONENTE A AGREGAR
	JButton boton1 = new JButton("Cancelar");
	JLabel etiqueta1 = new JLabel("un letrero");

	//establezco un tamao
	miFrame.setSize(200,200);

        Container contenedor = miFrame.getContentPane();
        boton1.setVisible(true);
        boton1.setEnabled(true);

        contenedor.setLayout(new FlowLayout());
        contenedor.add(etiqueta1);
        contenedor.add(boton1);

	//Tecnica para centrado de frame
	Dimension framesize = miFrame.getSize();
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	int centroX  = screenSize.width/2;
	int centroY  = screenSize.height/2;
	int ancho  = framesize.width/2;
	int alto  = framesize.height/2;
	miFrame.setLocation((centroX -ancho), (centroY - alto));

	//Tcnica para Cerrar la ventana
        miFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

	//establezco visible el componente	
	miFrame.setVisible(true);
	} // fin del main

}
