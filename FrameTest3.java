/**
 * FrameTest3.java
 * 
 */

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class FrameTest3 {

	/**
	 * Esta es una clase desplega un Frame y le agrega componentes con un FlowLayout
         * Registrando los eventos entre ellos
	 * @version 1.0
	 * @author Ivn Fuentes Quiroz
 	 */

	static JButton boton1 = new JButton("Boton 1");
	static JButton boton2 = new JButton("Boton 2");

	public static void main(String[] args) {
	JFrame miFrame = new JFrame("Cawobunga!!!");
	JLabel letrero = new JLabel("escoge uno");
	
	WindowListener w = new WindowAdapter() {
 		public void windowClosing(WindowEvent evt) {
                	exitForm(evt);
            	}
	};

	//Listener para cerrar la ventana
	miFrame.addWindowListener(w);

	miFrame.setSize(100,200);

	//Obtengo el contenedor para agregar componentes
	Container contenedor = miFrame.getContentPane();
	boton1.setVisible(true);
	boton1.setEnabled(true);
	
	boton2.setVisible(true);
	boton2.setEnabled(true);
	
	//Agrego una duistribucin
	contenedor.setLayout(new FlowLayout());
	contenedor.add(letrero);
	contenedor.add(boton1);
	contenedor.add(boton2);


	//AGREGUE OTRO COMPONONTE BOTON Y REGISTRELO AL LISTENER CORRESPONDIENTE
	//HABILITE LOS COMENTARIOS Y REALICE LO NECESARIO PARA MOSTRAR QUE BOTON SE PULSO
	//CON LOS METODOS "metodoBonton1" y "metodoBonton2"


	//Instancia de Listener para el Boton
	ActionListener l = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
	if (e.getSource() == boton1)
		metodoBoton1();
	else
		metodoBoton2();
		}
	};

	//Registro el Listener al boton
	boton1.addActionListener(l);
	boton2.addActionListener(l);


	//Tecnica para centrado de frame
	Dimension framesize = miFrame.getSize();
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	int centroX  = screenSize.width/2;
	int centroY  = screenSize.height/2;
	int ancho  = framesize.width/2;
	int alto  = framesize.height/2;
	miFrame.setLocation((centroX -ancho), (centroY - alto));


	//Pongo el Frame Visible
	miFrame.setVisible(true);


	//miFrame.pack();
	} //fin del main

	/**
	 * Metodo que cierra el frame desde le boton de close (X) 
	 * @param el evento de tipo WindowEvent que se registra
	 */

	private static void exitForm(WindowEvent evt) {
        	System.exit(0);
	}

	private static void metodoBoton1() {
		System.out.println("El Boton1 fue pulsado");
	}

	private static void metodoBoton2() {
		System.out.println("El Boton2 fue pulsado");
	}

}
