/**
 * FrameTest4.java
 * 
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FrameTest4 {

	/**
	 * Esta es una clase desplega un Frame y le agrega componentes en un Panel
         * con un BorderLayout definido al momento de agregarlo
	 * @version 1.0
	 * @author Ivn Fuentes Quiroz
 	 */

	public static void main(String[] args) {
	
           	JFrame miFrame = new JFrame("Cawobunga!!!");

		//Establezco el tamao
           	miFrame.setSize(400, 400); 

		//Tcnica para centrar el frame
		Dimension frameSize = miFrame.getSize();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		miFrame.setLocation((screenSize.width - frameSize.width)/2, 
				         (screenSize.height - frameSize.height)/2);

		//AGREGUE VARIOS COMPONENTES Y UBIQUELOS EN LAS DIFERENTES
		//REGIONES DEL BORDERLAYOUT



		WindowListener w = new WindowAdapter() {
 			public void windowClosing(WindowEvent evt) {
        	        	exitForm(evt);
            		}
		};

		//Listener para cerrar la ventana
		miFrame.addWindowListener(w);

		Container contentPane = miFrame.getContentPane( );
		JLabel stuff = new JLabel("Soy una etiqueta");
		contentPane.add(stuff, BorderLayout.NORTH);
		
		JLabel stuff1 = new JLabel("Soy una etiqueta");
		contentPane.add(stuff1, BorderLayout.SOUTH);
		
		JLabel stuff2 = new JLabel("Soy una etiqueta");
		contentPane.add(stuff2, BorderLayout.EAST);
		
		JLabel stuff3 = new JLabel("Soy una etiqueta");
		contentPane.add(stuff3, BorderLayout.WEST);
		
		//JLabel stuff4 = new JLabel("Soy una etiqueta");
		//contentPane.add(stuff4, BorderLayout.CENTER);

		//Muestro el frame en Pantalla
           	miFrame.setVisible(true);
      	}

	/**
	 * Metodo que cierra el frame desde le boton de close (X) 
	 * @param el evento de tipo WindowEvent que se registra
	 */

	private static void exitForm(WindowEvent evt) {
        	System.exit(0);
	}


}
