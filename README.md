/*********************************************************************************

                            Preguntas Laboratorio 8

                            Juan Pablo Ángel Parra Arévalo

                            Fundación Universitaria San Mateo
                            Facultad de Ingeniería y Afines
                            Ingeiería de Sistemas

                            Programación Orientada a Objetos

*********************************************************************************/

Punto 2:

a. Nótese que se agregan al contenedor la etiqueta y el botón, pero no se muestran, al
mismo tiempo ¿Porqué y que lo resolvería?

Porque falta la istrucción "add" para añadir el botón.
